
<?php



require_once("../../../vendor/autoload.php");

use App\City\City;

$objBookTitle  =  new City();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->viewSingle("obj");


include_once("header.php");


?>



            <div class="user-dashboard">
                <h1>Create</h1>
                <div class="row">
                    <div class="col-md-12">

                        <form role="form" action="edit_pro.php" method="post">
                            <div class="form-group">
                                <label for="tt"></label>
                                <input type="hidden"   value="<?php echo $oneData->id ?>" name="id">
                                <input type="text" name="name" id="first_name" class="form-control input-sm" value="<?php echo $oneData->name ?>">
                            </div>
                            <div class="form-group">
                                <select name="city_name" required>
                                    <option>Select City</option>
                                    <option value="Chittagong" <?php if($oneData->city_name=="Chittagong")echo "selected" ;?>>Chittagong</option>
                                    <option value="Dhaka" <?php if($oneData->city_name=="Dhaka")echo "selected" ;?>>Dhaka</option>
                                    <option value="Noakhali" <?php if($oneData->city_name=="Noakhali")echo "selected" ;?>>Noakhali</option>
                                    <option value="Khulna" <?php if($oneData->city_name=="Khulna")echo "selected" ;?>>Khulna</option>
                                    <option value="Shylet" <?php if($oneData->city_name=="Shylet")echo "selected" ;?>>Shylet</option>
                                    <option value="Rajshahi" <?php if($oneData->city_name=="Rajshahi")echo "selected" ;?>>Rajshahi</option>
                                </select></div>
                            <input type="submit" value="Update" class="btn btn-info btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- Modal -->
<div id="add_project" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>