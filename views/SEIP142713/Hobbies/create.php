<?php


require_once("../../../vendor/autoload.php");

use App\Message\Message;

//echo Message::getMessage();


include_once("header.php");
?>



            <div class="user-dashboard">
                <h1>Create</h1>
                <div class="row">
                    <div class="col-md-12">

                        <?php

                        if(isset($_SESSION['message'])) {?>

                            <div class="alert alert-success">
                                <?php
                                echo $_SESSION['message'];
                                ?>
                            </div>
                        <?php }
                        ?>



                        <form role="form" action="store.php" method="post">
                            <div class="form-group">
                                <label for="tt"></label>
                                <input type="text" name="name" id="name" class="form-control input-sm" placeholder="Enter your name">
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby1" id="hobby1"  value="Gardening">Gardening
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby2" id="hobby2" value="Novel Reading">Novel Reading
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby3" id="hobby3" value="Cycling">Cycling
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby4" id="hobby4" value="Traveling">Traveling
                            </div>
                            <input type="submit" value="Create" class="btn btn-info btn-block">
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- Modal -->
<div id="add_project" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>


