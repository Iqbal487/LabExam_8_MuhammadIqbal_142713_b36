
<?php




require_once("../../../vendor/autoload.php");

use App\Hobbies\Hobbies;

$objBookTitle  =  new Hobbies();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->viewSingle("obj");


$ah = explode(",",$oneData->hobbies);


include_once("header.php");


?>



            <div class="user-dashboard">
                <h1>Create</h1>
                <div class="row">
                    <div class="col-md-12">



                        <form role="form" action="store.php" method="post">
                            <div class="form-group">
                                <label for="tt"></label>
                                <input type="hidden"   value="<?php echo $oneData->id ?>" name="id">
                                <input type="text" name="name" id="name" class="form-control input-sm" value="<?php echo $oneData->name; ?>">
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby1" id="hobby1"  value="Gardening" <?php if(in_array("Gardening",$ah)){echo "checked"; } ?>>Gardening
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby2" id="hobby2" value="Novel Reading"  <?php if(in_array("Novel Reading",$ah)){echo "checked"; } ?>>Novel Reading
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby3" id="hobby3" value="Cycling"  <?php if(in_array("Cycling",$ah)){echo "checked"; } ?>>Cycling
                            </div>
                            <div class="form-group" style="text-align: left">
                                <input type="checkbox" name="hobby4" id="hobby4" value="Traveling"  <?php if(in_array("Traveling",$ah)){echo "checked"; } ?>>Traveling
                            </div>
                            <input type="submit" value="Create" class="btn btn-info btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- Modal -->
<div id="add_project" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>