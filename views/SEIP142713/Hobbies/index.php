

<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;


use App\Hobbies\Hobbies;

$objBookTitle  =  new Hobbies();

$allData = $objBookTitle->index("obj");

//print_r($allData);


$serialId = 1;



include_once("header.php");
?>



            <div class="user-dashboard">
                <h1>Hello, JS</h1>
                <div class="row">
                    <div class="col-md-12">

                        <?php if(isset($_SESSION['message'])) {?>

                        <div class="alert alert-success">
                        <?php
                            echo Message::message();
                            ?>
                            </div>
                       <?php }
                        session_destroy();
                        ?>


                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($allData as $sigleData){
                                echo "<tr>";
                                echo "<td>".$serialId."</td>";
                                echo "<td>".$sigleData->id."</td>";
                                echo "<td>".$sigleData->name."</td>";
                                echo "<td>".$sigleData->hobbies."</td>";




                                echo "<td>";

                                echo "<a href='view.php?id=$sigleData->id'><button class='btn btn-success'><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> View</button></a> ";
                                echo "<a href='edit.php?id=$sigleData->id'><button class='btn btn-primary'><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Edit</button></a> ";
                                echo "<a href='delete.php?id=$sigleData->id'><button class='btn btn-danger'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> Delete</button></a> ";
                                echo "<a href='trash.php?id=$sigleData->id'><button class='btn btn-warning'><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Trash</button></a> ";


                                echo "</td>";
                                echo "</tr>";

                                $serialId++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- Modal -->
<div id="add_project" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>