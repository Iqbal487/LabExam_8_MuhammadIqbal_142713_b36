
<?php



require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;

$objBookTitle  =  new BookTitle();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->viewSingle("obj");


include_once("header.php");


?>



            <div class="user-dashboard">
                <h1>Create</h1>
                <div class="row">
                    <div class="col-md-12">

                        <form role="form" action="edit_pro.php" method="post">
                            <div class="form-group">
                                <label for="tt"></label>
                                <input type="hidden"   value="<?php echo $oneData->book_id ?>" name="id">
                                <input type="text" name="book_title" id="first_name" class="form-control input-sm" value="<?php echo $oneData->book_title ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="author_name" id="email" class="form-control input-sm" placeholder="Author name" value="<?php echo $oneData->author_name ?>">
                            </div>
                            <input type="submit" value="Update" class="btn btn-info btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<!-- Modal -->
<div id="add_project" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Project</h4>
            </div>
            <div class="modal-body">
                <input type="text" placeholder="Project Title" name="name">
                <input type="text" placeholder="Post of Post" name="mail">
                <input type="text" placeholder="Author" name="passsword">
                <textarea placeholder="Desicrption"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="add-project" data-dismiss="modal">Save</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>