<?php

namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture_link;


    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVaribaleData=NULL)
    {
        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        if(array_key_exists("profile_photo",$postVaribaleData))
        {
            $this->picture_link = $postVaribaleData['profile_photo'];
        }




    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->picture_link);
        $sql = "INSERT into profile_picture(name,profile_picture) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }

        Utility::redirect('index.php');


    }



    public function index($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from profile_picture WHERE status=1 ORDER BY id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function viewSingle($fetchMode='ASSOC'){

        $sql = 'SELECT * from profile_picture where id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function edit(){

        $sql = "update profile_picture SET name ='". $this->name."', profile_picture = '".$this->picture_link."' where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been updated successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been updated successfully :(");

        Utility::redirect('index.php');


    }
///delete
    public function delete(){

        $sql = "delete from profile_picture where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been deleted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been deleted successfully :(");

        Utility::redirect('index.php');


    }


//trash
    public function trash(){

        $sql = "update profile_picture SET  status = 0 where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved tp trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved to trash successfully :(");

        Utility::redirect('index.php');


    }



    //view trash


    public function viewTrash($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from profile_picture WHERE status=0');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    //recover trash item to main item


//trash
    public function recover(){

        $sql = "update profile_picture SET  status = 1 where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved from trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved from trash successfully :(");

        Utility::redirect('index.php');


    }




}//end of book title class