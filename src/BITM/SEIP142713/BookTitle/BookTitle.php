<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class BookTitle extends DB
{
    public $id;
    public $title;
    public $author;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVaribaleData=NULL)
    {
       if(array_key_exists("id",$postVaribaleData))
       {
           $this->id = $postVaribaleData['id'];
       }
        if(array_key_exists("book_title",$postVaribaleData))
        {
            $this->title = $postVaribaleData['book_title'];
        }
        if(array_key_exists("author_name",$postVaribaleData))
        {
            $this->author = $postVaribaleData['author_name'];
        }

    }//end of set data
  /*  public function store()   //basic code
    {
        $sql = "INSERT into book_title(book_title,author_name) VALUES ('$this->title','$this->author')";
        $STH = $this->dbh->prepare($sql);
        $STH->execute();
    }//end of store*/

    public function store()
    {
        $arrData = array($this->title,$this->author);
        $sql = "INSERT into book_title(book_title,author_name) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");

        Utility::redirect('index.php');

    }



    public function index($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from book_title WHERE status=1 ORDER BY book_id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function viewSingle($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where book_id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function edit(){

        $sql = "update book_title SET book_title ='". $this->title."', author_name = '".$this->author."' where book_id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been updated successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been updated successfully :(");

        Utility::redirect('index.php');


    }
///delete
    public function delete(){

        $sql = "delete from book_title where book_id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been deleted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been deleted successfully :(");

        Utility::redirect('index.php');


    }


//trash
    public function trash(){

        $sql = "update book_title SET  status = 0 where book_id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved tp trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved to trash successfully :(");

        Utility::redirect('index.php');


    }



    //view trash


    public function viewTrash($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from book_title WHERE status=0');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    //recover trash item to main item


//trash
    public function recover(){

        $sql = "update book_title SET  status = 1 where book_id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved from trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved from trash successfully :(");

        Utility::redirect('index.php');


    }




}//end of book title class