<?php

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class Hobbies  extends DB
{
    public $id;
    public $name;
    public $hobbies=array();
    public $hobby;
    public $hobby2;


    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }
    public function setData($postVaribaleData=NULL)
    {


        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        for ($i=1;$i<=4;$i++)
        {
            if(array_key_exists("hobby".$i,$postVaribaleData))
            {
                $this->hobby = $postVaribaleData['hobby'.$i];
                array_push($this->hobbies,$this->hobby);
            }
        }

        $this->hobby2=implode(',',$this->hobbies);

    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->hobby2);
        $sql = "INSERT into hobbies(name,hobbies) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }

        Utility::redirect('index.php');


    }
    public function index($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from hobbies WHERE status=1 ORDER BY id DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function viewSingle($fetchMode='ASSOC'){

        $sql = 'SELECT * from hobbies where id='.$this->id;

        $STH = $this->dbh->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function edit(){

        $sql = "update hobbies SET name ='". $this->name."', hobbies = '".$this->hobby2."' where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been updated successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been updated successfully :(");

        Utility::redirect('index.php');


    }
///delete
    public function delete(){

        $sql = "delete from hobbies where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been deleted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been deleted successfully :(");

        Utility::redirect('index.php');


    }


//trash
    public function trash(){

        $sql = "update hobbies SET  status = 0 where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved tp trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved to trash successfully :(");

        Utility::redirect('index.php');


    }



    //view trash


    public function viewTrash($fetchMode='ASSOC'){


        $STH = $this->dbh->query('SELECT * from hobbies WHERE status=0');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    //recover trash item to main item


//trash
    public function recover(){

        $sql = "update hobbies SET  status = 1 where id= ".$this->id."";

        $STH = $this->dbh->prepare($sql);


        $arrOneData  = $STH->execute();


        if($arrOneData)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been moved from trash successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been moved from trash successfully :(");

        Utility::redirect('index.php');


    }




}//end of book title class