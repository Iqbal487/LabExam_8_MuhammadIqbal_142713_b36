-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 10:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `anik_atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(10) NOT NULL,
  `name` text NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`, `status`) VALUES
(1, 'Fariha', '10/12/1991', 1),
(2, 'Emon', '23/10/1998', 1),
(3, 'dsgsdgds', '2016-11-14', 1),
(4, 'rteruteruty', '2016-11-08', 1),
(5, 'gfdgdfg', '2016-11-25', 1),
(6, 'dsdfsfdf', '2016-11-09', 1),
(7, 'jhdfjshdjfhsd', '2016-11-10', 1),
(9, '', '', 1),
(12, 'trgfrgbfsd', '2016-11-15', 0),
(13, 'anik', '2016-11-10', 0),
(14, 'anik', '2016-11-11', 0),
(15, 'wqer13wqrqewre', '2016-11-08', 0),
(16, 'saiful', '2009-07-09', 0),
(17, 'wdeftre', '2016-11-09', 0),
(18, 'rewtreqwt', '2016-11-11', 0),
(19, '231q4rwqerewqrew', '2016-11-03', 0),
(20, 'erewrwqqwrweqrweqrweqrewqr', '2016-11-10', 0),
(21, 'trewt3rtge', '2016-11-02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`book_id` int(10) NOT NULL,
  `book_title` text NOT NULL,
  `author_name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`book_id`, `book_title`, `author_name`, `status`) VALUES
(15, 'reatfwetgrewfg', 'erwrtgewtewtrewterwtrewtrrsf', 0),
(18, 'ffdsafdsfdsdfs', 'dsfdsffddfsafdsafds', 0),
(19, 'dsfadfsadfssdfadfs', 'dsffdsfds', 0),
(21, 'ghhgh', 'ghghgghgf', 0),
(22, 'rtgwrfgdfs', 'fdhdhgfhd', 0),
(23, 'bdtrgrh', 'fdhhghgghhhh', 1),
(24, 'hfdhgfdsgdsfgfdsgfsfdg', 'ghdhd', 1),
(26, 'fgtrgew', 'trwetrwetwet', 0),
(28, 'rerwtrew', 'rewtrewtewrt', 0),
(30, 'dfsdaf', 'dsagfdsgfs', 0),
(31, 'dfsdsfadsa', 'dsfafdsa', 1),
(32, 'dsfafsdasda', 'dsfadsfadsadsafsdadsa', 1),
(33, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(34, 'dsfafsdasda', 'dsfadsfadsadsafsdadsa', 1),
(35, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(36, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(37, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(38, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(39, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(40, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(41, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(42, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(43, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(44, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(45, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(46, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(47, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(48, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(49, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(50, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(51, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(52, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(53, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(54, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(55, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(56, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(57, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(58, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(59, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(60, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(61, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(62, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(63, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(64, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(65, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(66, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(67, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(68, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(69, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(70, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(71, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(72, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(73, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(74, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(75, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(76, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(77, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(78, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(79, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(80, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(81, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(82, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(83, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(84, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(85, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(86, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(87, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(88, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(89, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(90, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(91, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(92, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(93, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(94, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(95, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(96, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(97, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(98, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(99, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(100, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(101, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(102, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(103, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(104, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(105, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(106, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(107, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(108, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(109, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(110, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(111, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(112, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(113, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(114, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(115, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(116, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(117, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(118, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(119, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(120, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(121, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(122, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(123, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(124, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1),
(125, 'dsfadsafdsafdsa', 'adsfdsafasdfds', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(10) NOT NULL,
  `name` text NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `status`) VALUES
(5, 'wasif', 'Munsigonj', 0),
(7, 'dsafdsafdsa', 'sdafdsadsa', 0),
(10, 'tryhdh', 'Dhaka', 1),
(11, 'fdsdafdsa', 'Shylet', 1),
(12, 'dfagadfga', 'Shylet', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`email_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `email`, `password`) VALUES
(1, 'someone@gmail.com', '7ac60b3a8bbe6eb22fc6c129d0b1d9dd'),
(2, 'example@yahoo.com', '8e0dfe712821d53026a7ba258bbc970f'),
(3, 'bristybilash16@gmail.com', '11f4d0712e8278ac1934f533e51bf6b3'),
(4, 'jdsgdfh@yahoo.com', '352751383f8e3dd5f08905fff9ee9a6e');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(10) NOT NULL,
  `name` text NOT NULL,
  `gender` varchar(30) NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `status`) VALUES
(3, 'derwerwe', 'Male', 1),
(4, 'ueyruyer bhdfdghfg', 'Female', 0),
(5, 'ttt', 'Female', 0),
(6, 'sdafadsfdsa', 'Female', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobbies` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `status`) VALUES
(1, 'Fariha', 'Book reading,Gardening,Traveling', 1),
(2, 'Emon', 'Traveling', 1),
(3, 'ueedgfhdhvcvvxnbcv', 'Gardening,Novel Reading,Traveling', 1),
(4, 'ueedgfhdhvcvvxnbcv', 'Gardening,Novel Reading,Traveling', 1),
(5, 'rtuirutiertoiegjjbg', 'Cycling,Traveling', 1),
(7, 'trgfdfdgfdhgf', 'Gardening,Novel Reading', 0),
(8, 'trgfdfdgfdhgf2222222222222222', 'Novel Reading,Cycling', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile_picture` varchar(300) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `status`) VALUES
(3, 'iruiewhefhdsfbvbcnxb', '1478940477car.jpg', 1),
(4, 'qqqqqqqqqqqq', '1478942494image001.png', 1),
(5, 'hgfhfhfgh', '1478942546car.jpg', 1),
(6, 'cbvcbxvb', '1478942580download.jpg', 1),
(7, 'ttt', '1478943796car.jpg', 0),
(14, 'anik', '1479553163as.jpg', 0),
(16, 'edfadsdsa', '1479715621Fairy_Tale_3D-Street-Art-Painting-2.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
`summary_organizaton_id` int(10) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `summary` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`summary_organizaton_id`, `organization_name`, `summary`) VALUES
(1, 'BITM', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, '),
(2, 'BASIS', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(3, 'Basis', 'yeurytueyruefhvbbvnb\r\nkjgerurtuerhgfdhbhgfgbd\r\nnfgfndjgdjfgj'),
(4, 'bitm', 'djhfsdfyueyfdhvb djhgzsdhfgshgfhds'),
(5, 'ttt', '1\r\n2\r\n3\r\n4\r\n5'),
(6, 'fgsgfdsg', 'gfdsgfdsg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
 ADD PRIMARY KEY (`summary_organizaton_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `email_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
MODIFY `summary_organizaton_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
